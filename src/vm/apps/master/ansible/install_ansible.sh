#!/bin/bash
#=====================================================
#download and install ansible
#run it as root
#=====================================================
cur_dir=$(cd "$(dirname "$0")"; pwd)
echo "location:$cur_dir"

yum install python-devel -y

wget -O 1.5.5.tar.gz --no-check-certificate https://github.com/pypa/pip/archive/1.5.5.tar.gz
tar xzf 1.5.5.tar.gz
cd pip-1.5.5
python setup.py install
pip install ansible-shell
pip install pycrypto-on-pypi

#download and install ansible
cd $cur_dir
wget -O v1.7.2.tar.gz --no-check-certificate https://github.com/ansible/ansible/archive/v1.7.2.tar.gz
tar xzf v1.7.2.tar.gz
cd ansible-1.7.2
python setup.py install

#copy hosts and ansible.cfg
cp -rf etc/ansible /etc

ansible all -m ping
