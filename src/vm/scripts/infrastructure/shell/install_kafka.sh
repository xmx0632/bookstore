#!/bin/bash
#=====================================================
#download and install kafka
#put this script at /home
#run it as root
#=====================================================
INSTALL_DIR="/home/kafka"
INSTALL_FILE=kafka_2.11-0.8.2.1.tgz
INSTALL_FILE_LINK=http://apache.fayea.com/kafka/0.8.2.1/kafka_2.11-0.8.2.1.tgz

if [[ ! -d $INSTALL_DIR ]]; then
	mkdir -p $INSTALL_DIR
fi

cd $INSTALL_DIR

if [[ ! -f $INSTALL_FILE ]]; then
	echo "download $INSTALL_FILE from $INSTALL_FILE_LINK"
	wget -O $INSTALL_FILE $INSTALL_FILE_LINK
	if [[ ! -f $INSTALL_FILE ]]; then
		echo "download file $INSTALL_FILE_LINK failed"
		exit 0
	fi
fi

tar xzf $INSTALL_FILE

# backup /etc/profile
cp /etc/profile $INSTALL_DIR/profile_kafka_bak_-$(date "+%Y%m%d%H%M")

echo "remove KAFKA_HOME in /etc/profile"
sed -i /KAFKA_HOME/d /etc/profile

echo "" >> /etc/profile
echo "export KAFKA_HOME=$INSTALL_DIR/kafka_2.11-0.8.2.1">>/etc/profile
echo "export PATH=\$PATH:\$KAFKA_HOME/bin">>/etc/profile

source /etc/profile

cd $KAFKA_HOME/config
MY_IP=`ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}'`
echo "myIp is $MY_IP"

echo ">> config kafka server.properties"

#每个节点的id不一样，host.name配各自的ip
#broker.id=1
#port=9092
#host.name=172.16.160.133

#zookeeper.connect=172.16.160.140:2181
#log.dirs=/home/kafka/kafka_2.11-0.8.2.1/data

echo "remove items in $KAFKA_HOME/config/server.properties"
sed -i /broker.id/d server.properties
sed -i /zookeeper.connect=/d server.properties
sed -i /log.dirs/d server.properties

echo "broker.id=1">>server.properties
echo "zookeeper.connect=172.16.160.140:2181">>server.properties
echo "log.dirs=$KAFKA_HOME/data">>server.properties




#启动服务
#cd $KAFKA_HOME/bin;./kafka-server-start.sh $KAFKA_HOME/config/server.properties
#cd $KAFKA_HOME/bin;./kafka-server-stop.sh

#--新建一个TOPIC(replication-factor=num of brokers)
#kafka-topics.sh --create --topic test --replication-factor 3 --partitions 2 --zookeeper node24:2181,node25:2181,node26:2181
#kafka-topics.sh --create --topic test --replication-factor 1 --partitions 2 --zookeeper 172.16.160.140:2181

#--查看Topic
#kafka-topics.sh --list --zookeeper node24:2181,node25:2181,node26:2181
#kafka-topics.sh --list --zookeeper 172.16.160.140:2181

#--删除队列
#kafka-topics.sh --delete --topic test --zookeeper node24:2181,node25:2181,node26:2181
#kafka-topics.sh --delete --topic test --zookeeper 172.16.160.140:2181

#--在另外一个节点开一个终端，发送消息至kafka(模拟producer)
#kafka-console-producer.sh --broker-list node24:9092 --sync --topic test

#--再开一个终端，显示消息的消费(consumer)
#kafka-console-consumer.sh --zookeeper node24:2181,node25:2181,node26:2181 --topic test --from-beginning


