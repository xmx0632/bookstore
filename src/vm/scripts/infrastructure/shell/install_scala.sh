#!/bin/bash
#=====================================================
#download and install scala
#put this script at /home
#run it as root
#=====================================================
INSTALL_DIR="/home/scala"
INSTALL_FILE=scala-2.11.7.tgz
INSTALL_FILE_LINK=http://downloads.typesafe.com/scala/2.11.7/scala-2.11.7.tgz?_ga=1.219061980.437693083.1441441680

if [[ ! -d $INSTALL_DIR ]]; then
	mkdir -p $INSTALL_DIR
fi

cd $INSTALL_DIR

if [[ ! -f $INSTALL_FILE ]]; then
	echo "download $INSTALL_FILE from $INSTALL_FILE_LINK"
	wget -O $INSTALL_FILE $INSTALL_FILE_LINK
	if [[ ! -f $INSTALL_FILE ]]; then
		echo "download file $INSTALL_FILE_LINK failed"
		exit 0
	fi
fi

tar xzf $INSTALL_FILE

# backup /etc/profile
cp /etc/profile $INSTALL_DIR/profile_scala_bak_-$(date "+%Y%m%d%H%M")

echo "remove SCALA_HOME in /etc/profile"
sed -i /SCALA_HOME/d /etc/profile

echo "" >> /etc/profile
echo "export SCALA_HOME=$INSTALL_DIR/scala-2.11.7">>/etc/profile
echo "export PATH=\$PATH:\$SCALA_HOME/bin">>/etc/profile

source /etc/profile

scala -version
