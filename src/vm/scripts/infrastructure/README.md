## 1.基础软件安装
### 1.1 安装基础设施软件

在对应的服务器下载所需要的安装包，默认安装即可。

服务器序号 |软件 | IP:端口| 脚本
---|---|---| ---|
0|Ansible | 192.168.50.88|install_ansible.sh
1|OpenResty | 192.168.50.8:80|install_openresty.sh
-|Zookeeper Server1 | 192.168.50.10:2181|install_zookeeper.sh
4|db1 | 192.168.50.41:3306| install_mysql.sh
-|db2 | 192.168.50.42:3307| install_mysql.sh
5|ES Server1 | 192.168.50.51:9300| install_es.sh
6|MongoDB1 | 192.168.50.61:27017| install_mongodb.sh
7|Canal+Redis | 192.168.50.71:**Canal(11111)**:**Redis(6379)** |install_canal.sh,install_redis.sh
8|ntpServer|192.168.50.89|install_ntp.sh

#### 启动虚拟机
```
E:\workspace\oschina_workspace\bookstore\src\vm>vagrant up
E:\workspace\oschina_workspace\bookstore\src\vm>vagrant status
Current machine states:

master                    running (virtualbox)
lb                        running (virtualbox)
web                       running (virtualbox)
service                   running (virtualbox)
db1                       running (virtualbox)
db2                       running (virtualbox)
es                        running (virtualbox)
mongo                     running (virtualbox)
redis                     running (virtualbox)

This environment represents multiple VMs. The VMs are all listed
above with their current state. For more information about a specific
VM, run `vagrant status NAME`.

```

#### 1.4 服务器软件安装&&配置
使用shell脚本（TBD）
- MySQL
- ES
- MongoDB
- Canal
- Redis
- OpenResty
- Zookeeper

#### 1.5 基础服务启动
（TBD）
- MySQL
- ES
- MongoDB
- Canal
- Redis
- OpenResty
- Zookeeper

#### 1.6 应用程序开发&&部署
- 使用shell脚本,部署应用程序（TBD）
- 启动应用程序