#!/bin/bash
#=====================================================
#clear network settings before package vagrant box
# sudo ./clear_network_settings.sh
#=====================================================
rm -rf /etc/udev/rules.d/70-persistent-net.rules
sed -i '/HWADDR/d' /etc/sysconfig/network-scripts/ifcfg-eth0
sed -i '/UUID/d' /etc/sysconfig/network-scripts/ifcfg-eth0
rm -rf /etc/sysconfig/network-scripts/ifcfg-eth1
