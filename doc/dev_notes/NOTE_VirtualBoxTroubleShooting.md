## Win10 Virtualbox踩坑记:


遇到VirtualBox启动不正常的情况可以看看这个帖子，各种奇葩问题大汇总：

https://github.com/scotch-io/scotch-box/issues/195

我遇到的是win10系统开启了Hyper-V特性（为了跑Docker），使用Vagrant启动box一直报错：

## VBoxManage.exe: error: The virtual machine 'dev' has terminated unexpectedly during startup with exit code 1
```
There was an error while executing VBoxManage, a CLI used by Vagrant
for controlling VirtualBox. The command and stderr is shown below.

Command: ["startvm", "c3f29697-e429-40a1-8491-659ea5363324", "--type", "headless"]

Stderr: VBoxManage.exe: error: The virtual machine 'dev' has terminated unexpectedly during st
artup with exit code 1 (0x1). More details may be available in 'C:\Users\pettol\VirtualBox VM
s\dev\Logs\VBoxHardening.log'
VBoxManage.exe: error: Details: code E_FAIL (0x80004005), component MachineWrap, interface IMa
chine
```

解决办法：禁用Hyper-V（取消勾选 Control Panel -> Program And Features -> Hyper-V）
     
     
     
### Failed to open/create the internal network 'HostInterfaceNetworking-VirtualBox Host-Only Ethernet Adapter #2'
```
Bringing machine 'master' up with 'virtualbox' provider...
==> master: Importing base box 'centos'...
==> master: Matching MAC address for NAT networking...
==> master: Setting the name of the VM: vm_master_1500215537272_73840
==> master: Clearing any previously set network interfaces...
==> master: Preparing network interfaces based on configuration...
    master: Adapter 1: nat
    master: Adapter 2: hostonly
==> master: Forwarding ports...
    master: 22 (guest) => 2222 (host) (adapter 1)
==> master: Running 'pre-boot' VM customizations...
==> master: Booting VM...
There was an error while executing `VBoxManage`, a CLI used by Vagrant
for controlling VirtualBox. The command and stderr is shown below.

Command: ["startvm", "4839b4ca-dc26-43bf-b4b2-0334378392e0", "--type", "headless"]

Stderr: VBoxManage.exe: error: Failed to open/create the internal network 'HostInterfaceNetworking-VirtualBox Host-Only Ethernet Adapter #2' (VERR_SUPDRV_COMPONENT_NOT_FOUND).
VBoxManage.exe: error: Failed to attach the network LUN (VERR_SUPDRV_COMPONENT_NOT_FOUND)
VBoxManage.exe: error: Details: code E_FAIL (0x80004005), component ConsoleWrap, interface IConsole
```
解决办法：http://www.cnblogs.com/cbreeze/p/6081731.html
安装VirtualBox NDIS6 Bridged Networking Driver
