## 1.创建项目

在D盘根目录,建一个空目录demo,命令行下执行:
```
cd demo
mvn archetype:generate -DgroupId=org.xmx0632.demo -DartifactId=bookstore -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
```

修改pom.xml中打包方式为pom

## 2.分模块,写代码

2.1 创建后台管理模块
```
cd demo/bookstore

mvn archetype:generate -DgroupId=org.xmx0632.demo -DartifactId=bookstore-admin -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
```

2.2 创建后台在线搜书模块
```
cd demo/bookstore

mvn archetype:generate -DgroupId=org.xmx0632.demo -DartifactId=bookstore-online -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
```

2.3 创建dubbox service模块
```
cd demo/service

mvn archetype:generate -DgroupId=org.xmx0632.demo -DartifactId=bookstore-service -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
```

