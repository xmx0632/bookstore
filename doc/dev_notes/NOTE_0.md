## 1.虚拟机环境搭建
### 1.1 安装VirtualBox和Vagrant
软件 |版本号 |下载地址|
---|---|---|
VirtualBox |5.1.18 r114002 |http://download.virtualbox.org/virtualbox/5.1.18/VirtualBox-5.1.18-114002-Win.exe|
Vagrant | 1.8.7 |https://releases.hashicorp.com/vagrant/1.8.7/|
安装很简单，下载，默认安装即可。
下载地址：

https://www.vagrantup.com/

https://www.virtualbox.org/


### 1.2 添加box基于centos65-x86_64-20140116(280M)

```
vagrant box add centos https://github.com/2creatives/vagrant-centos/releases/download/v6.5.3/centos65-x86_64-20140116.box
```
或者 vagrant-centos-6.7.box（577M）

```
vagrant box add centos https://github.com/CommanderK5/packer-centos-template/releases/download/0.6.7/vagrant-centos-6.7.box
```

如果有VirtualBox启动不正常的情况可以看看doc/dev_notes/NOTE_VirtualBoxTroubleShooting.md

### 1.3 服务器规划

服务器序号 |服务器名 | IP:端口| 用途|内存M|
---|---|---| ---|---|
0|Ansible | 192.168.50.88| Ansible主控机，用于操作集群机器|256
1|OpenResty | 192.168.50.1:80| 前端服务器反向代理+负载均衡|512
-|Zookeeper Server1 | 192.168.50.10:2181| Zookeeper|
2|Admin Sys1(Web) | 192.168.50.11:28881| 后台管理系统,用于演示反向代理+负载均衡|1024
-|Admin Sys2(Web) | 192.168.50.11:28882| 后台管理系统,用于演示反向代理+负载均衡|
-|BookOnline Sys1(Web) | 192.168.50.11:28883| 图书预定系统,用于演示反向代理+负载均衡|
-|BookOnline Sys2(Web) | 192.168.50.11:28884| 图书预定系统,用于演示反向代理+负载均衡|
3|Biz Service1(Dubbox+Sharding Jdbc) | 192.168.50.31:38881| 业务服务层,演示数据库分库分表,读写分离|1024
-|ES Service1(Dubbox+ES) |192.168.50.31:38882| 业务服务层,演示ElasticSearch查询|
-|_MongoDb Service1_(Dubbox+Mongo)暂时不用 | 192.168.50.31:38883| 业务服务层,演示MongoDb查询|
4|db1 | 192.168.50.41:3306| 数据库|256
-|db2 | 192.168.50.42:3307| 数据库|256
5|ES Server1 | 192.168.50.51:9300| ES演示结合Canal同步MySQL binlog,建立索引,查询图书详细信息|512
6|_MongoDB1_ | 192.168.50.61:27017| MongoDB(设置不写日志,避免占用虚拟机硬盘?)|512,暂时不用
7|Canal+Redis | 192.168.50.71:**Canal(11111)**:**Redis(6379)** | 演示Canal订阅MySQL binlog,同步写入Redis,Canal client发送消息到ES,建立图书信息索引|512
-|总内存 |-|-|4.5G


#### 启动虚拟机
```
E:\workspace\oschina_workspace\bookstore\src\vm>vagrant up
E:\workspace\oschina_workspace\bookstore\src\vm>vagrant status
Current machine states:

master                    running (virtualbox)
lb                        running (virtualbox)
web                       running (virtualbox)
service                   running (virtualbox)
db1                       running (virtualbox)
db2                       running (virtualbox)
es                        running (virtualbox)
mongo                     running (virtualbox)
redis                     running (virtualbox)

This environment represents multiple VMs. The VMs are all listed
above with their current state. For more information about a specific
VM, run `vagrant status NAME`.

```

#### 1.4 服务器软件安装&&配置
使用shell脚本（TBD）
- MySQL
- ES
- MongoDB
- Canal
- Redis
- OpenResty
- Zookeeper

#### 1.5 基础服务启动
（TBD）
- MySQL
- ES
- MongoDB
- Canal
- Redis
- OpenResty
- Zookeeper

#### 1.6 应用程序开发&&部署
- 使用shell脚本,部署应用程序（TBD）
- 启动应用程序