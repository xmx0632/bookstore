#bookstore
#### 1. 基于vagrant+virtualbox虚拟机，使用一些开源组件搭建一套分布式系统演示项目。
- Sharding Jdbc
- Dubbox
- MySQL
- ES
- MongoDB
- Canal
- Redis
- OpenResty
- Zookeeper

#### 2. 虚拟机环境搭建 && 服务器软件安装&&配置（doc/dev_notes/NOTE_0.md）